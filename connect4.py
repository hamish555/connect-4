import tkinter as tk
import time


def main():
    def restart(*args):
        root.destroy()
        main()

    def drop(color, column, *args):
        global player
        if height[column] != 0:
            tk.Label(root, bg=color).grid(column=column,
                                          row=height[column], ipadx=25,
                                          ipady=15, pady=5)
            height[column] -= 1
            if player == 'Blue':
                player = 'Red'
            else:
                player = 'Blue'

    global player
    player = 'Blue'
    height = {}
    for i in range(7):
        height[i] = 6

    root = tk.Tk()

    for i in range(7):
        tk.Button(root, text='Drop', command=lambda x=i:
                  drop(player, x)).grid(column=i, row=0)
    root.bind('1', lambda x: drop(player, 0))
    root.bind('2', lambda x: drop(player, 1))
    root.bind('3', lambda x: drop(player, 2))
    root.bind('4', lambda x: drop(player, 3))
    root.bind('5', lambda x: drop(player, 4))
    root.bind('6', lambda x: drop(player, 5))
    root.bind('7', lambda x: drop(player, 6))
    root.bind('r', restart)
    root.bind('R', restart)
    for x in range(6):
        x += 1
        for i in range(7):
            tk.Label(root, bg='Gray').grid(column=i, row=x,
                                           ipadx=25, ipady=15, pady=5, padx=5)
    root.mainloop()

main()
